import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router} from '@angular/router';
import { loginService} from '../services/apiCall.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  loading:boolean = false;
  loginErr:boolean= false;

  constructor(private formBuilder: FormBuilder,public Router:Router,public loginService:loginService,public toastr: ToastrService, vcr: ViewContainerRef) {

  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
  });
  }
      // convenience getter for easy access to form fields
      get f() { return this.registerForm.controls; }

      onSubmit() {
          this.submitted = true;
          this.loading = true;
          // stop here if form is invalid
          let obj={
            "email": this.registerForm.value.email,
            "password": this.registerForm.value.password
          }
          this.loginService.checkLogin(obj).subscribe(data=>{
            this.loading = false;
            localStorage.setItem('cine-auth-check',data['authToken']);
            this.toastr.success('success',data['message']);
            this.Router.navigate(['/invoice']);

          },err=>{
            this.loading = false;
              this.toastr.error('Error',err.error.message);
              if(err.error.statusCode == '401') {
                localStorage.removeItem('cine-auth-check');
                this.Router.navigate(['/login']);
              }
          });
          if (this.registerForm.invalid) {
              return;
          }
      }

}
