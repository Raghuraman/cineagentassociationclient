import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError, tap,debounceTime } from 'rxjs/operators';
import { CommmonServiceService } from './commmon-service.service';

@Injectable()

export class loginService {
    constructor(private http: HttpClient) { }
    checkLogin(data) {
        var respData = CommmonServiceService.noAuthReq('/login');
        return this.http.post(respData.url,data,respData.headers).pipe(map((res) => res));
    }
    logoutService() {
      var respData = CommmonServiceService.authReq('/logout');
      return this.http.post(respData.url,{},respData.headers).pipe(map((res) => res));
    }
    saveData(data) {
      var respData = CommmonServiceService.authReq('/add_bill');
      return this.http.post(respData.url,data,respData.headers).pipe(map((res) => res));
    }
    updateData(data,id) {
      var respData = CommmonServiceService.authReq('/bill/'+id);
      return this.http.put(respData.url,data,respData.headers).pipe(map((res) => res));
    }
    getHistory(data) {
      var respData;
      if(data.filter_by == "") {
        respData = CommmonServiceService.authReq('/billHistory/1?limit='+data.limit+'&offset='+data.offset);

      } else {
        respData = CommmonServiceService.authReq('/billHistory/1?limit='+data.limit+'&offset='+data.offset+'&filter_by='+data.filter_by);

      }
      return this.http.get(respData.url,respData.headers).pipe(map((res) => res));

    }
    getHistorySearch(data) {
      var respData;
      if(data.filter_by == "") {
        respData = CommmonServiceService.authReq('/billHistory/1?limit='+data.limit+'&offset='+data.offset);
        return this.http.get(respData.url,respData.headers).pipe(map((res) => res));
      } else {
       respData = CommmonServiceService.authReq('/billHistory/1?limit='+data.limit+'&offset='+data.offset+'&filter_by='+data.filter_by);
       return this.http.get(respData.url,respData.headers).pipe(debounceTime(1000),map((res) => res));
      }
    }
    getHistoryDetail(id){
      var respData = CommmonServiceService.authReq('/bill/'+id);
      return this.http.get(respData.url,respData.headers).pipe(map((res) => res));
    }
    deleteBill(id) {
      var respData = CommmonServiceService.authReq('/bill/'+id);
      return this.http.delete(respData.url,respData.headers).pipe(map((res) => res));
    }
}
