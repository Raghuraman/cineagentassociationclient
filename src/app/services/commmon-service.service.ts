import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CommmonServiceService {
    static rootUrl:string = 'http://api.cineagentassociations.com';
    static cdnUrl:string = "";

    static noAuthReq(url:string) {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      };
      return {
        url: this.rootUrl + url,
        headers: httpOptions
    }
  }
  static authReq(url:string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'authorization':localStorage.getItem('cine-auth-check')
      })
    };
    return {
      url: this.rootUrl + url,
      headers: httpOptions
  }
}
}
