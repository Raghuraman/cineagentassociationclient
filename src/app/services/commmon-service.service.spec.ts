import { TestBed, inject } from '@angular/core/testing';

import { CommmonServiceService } from './commmon-service.service';

describe('CommmonServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommmonServiceService]
    });
  });

  it('should be created', inject([CommmonServiceService], (service: CommmonServiceService) => {
    expect(service).toBeTruthy();
  }));
});
