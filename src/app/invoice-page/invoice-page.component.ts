import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator, MatSort } from '@angular/material';
import { DataTableDataSource } from './data-table-datasource';
import { ToastrService } from 'ngx-toastr';
import { loginService } from '../services/apiCall.service';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import * as moment from 'moment';
import { validateConfig } from '@angular/router/src/config';
@Component({
  selector: 'app-invoice-page',
  templateUrl: './invoice-page.component.html',
  styleUrls: ['./invoice-page.component.less']
})
export class InvoicePageComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  apiCallLoader= false;
  Editable = false;
  EditId = '';
  totalLength: 0;
  dataSource: '';
  displayedColumns = ['companyName', 'agentName', 'billDate', 'language', 'amount', 'Edit','Delete'];
  displayTime = ['9am to 6pm  1', '6pm to 2am  1', '10pm to 6am  1', '2pm to 10 pm  1', '2pm to 2am  2', '9am to 10pm 1.5', '9am to 2am  2', '6am to 2pm  1'];
  categoryDisp = ['Gents','Ladies','Gents characters','Ladies characters','Gents & Ladies','S.I','Police','Dialogue','Out of city additional wages',
  'Gents Spl characters','Ladies Spl characters','Up & Down Journey','Gents Charactor Cont. Basis','Ladies Charactor Cont. Basis',
  'Boys Charactor Cont. Basis','Girls Charactor Cont. Basis','Gents Audience Cont. Basis','Ladies Audience Cont. Basis','Boys Audience Cont. Basis',
  'Girls Audience Cont. Basis','Boys / Girls','Boys','Girls','Dance Movement','Group Struggling','Rain Shot','Bomb Shot','Others'];
  callsheetArr = [1, 1, 1, 1, 2, 1.5, 2, 1]
  selectionCategory = ['Gents Charactor Cont. Basis','Ladies Charactor Cont. Basis',
  'Boys Charactor Cont. Basis','Girls Charactor Cont. Basis','Gents Audience Cont. Basis','Ladies Audience Cont. Basis','Boys Audience Cont. Basis',
  'Girls Audience Cont. Basis','Up & Down Journey','Out of city additional wages','Dialogue','Dance Movement','Group Struggling','Rain Shot','Bomb Shot'];
  contractCateogary = ['Gents Charactor Cont. Basis','Ladies Charactor Cont. Basis',
  'Boys Charactor Cont. Basis','Girls Charactor Cont. Basis','Gents Audience Cont. Basis','Ladies Audience Cont. Basis','Boys Audience Cont. Basis',
  'Girls Audience Cont. Basis'];
  fromTime: '';
  toTime: '';
  SearchByName: '';
  crdate = new Date(moment().subtract(1, "months").format());
  tableFilter = {
    'fromDate': new FormControl(this.crdate),
    'toDate': new FormControl(new Date()),
    'offset': 0,
    'limit': 10,
    'filter_by': ''
  }
  selected = new FormControl(0);
  inoviceObj = {
    companyName: '',
    date: new FormControl(new Date()),
    agentName: '',
    language: '',
    entries: [
      {
        uniqueId: 1000,
        entryDate: new FormControl(new Date()),
        location: '',
        time: '',
        startTime: '',
        endTime: '',
        type: 'add',
        subEntries: [
          {
            category: '',
            otherCategory:'',
            qty: '',
            cost: '',
            callsheet: '',
            total: 0,
            type: 'add'
          }
        ]
      }
    ]
  }
  tmpDateFrom: any;
  tempDateTo: any;
  constructor(public Router: Router, public toastr: ToastrService, vcr: ViewContainerRef, public loginService: loginService) {
    this.SearchByName = '';
  }
  getServerData(event) {
    let obj = {
      'fromDate': this.tmpDateFrom,
      'toDate': this.tempDateTo,
      'offset': event.pageIndex * event.pageSize,
      'limit': 10,
      'filter_by': this.SearchByName
    }
    this.getDataTable(obj);
  }
  ngOnInit() {

    // this.dataSource =[
    //   {id: 1, name: 'Hydrogen'},
    //   {id: 2, name: 'Helium'},
    //   {id: 3, name: 'Lithium'},
    //   {id: 4, name: 'Beryllium'},
    //   {id: 5, name: 'Boron'},
    // ] ;
  }
  // changeTimeValue(event, obj) {
  //   var endTime;
  //   if (event.value == 'other') {
  //     console.log('from time',obj.fromTime);
  //     console.log('to Time',obj.toTime);
  //   } else {
  //     endTime = event.value.substring(event.value.length - 1);
  //     if (endTime == 5) {
  //       endTime = event.value.substring(event.value.length - 3);
  //       obj['callSh'] = endTime;
  //     } else {
  //       endTime = event.value.substring(event.value.length - 1);
  //       obj['callSh'] = endTime;
  //     }
  //   }
  // }
  getPagedData(event) {
    console.log('event trigger', event);
  }
  getCallsheet(obj, subobj) {
    if (obj.time == 'other') {
      console.log("Object request", obj);
      let FromType = obj.startTime.slice(-2);
      let ToType = obj.endTime.slice(-2);
      let fromTime = obj.startTime.slice(0, 2);
      let endTime = obj.endTime.slice(0, 2);
      var diffHrs;
      console.log('type', FromType, ToType);
      console.log('time', fromTime, endTime);
      if (FromType == ToType) {
        if (parseInt(fromTime) <= parseInt(endTime)) {
          diffHrs = parseInt(endTime) - parseInt(fromTime);
        } else if (parseInt(fromTime) > parseInt(endTime)) {
          let sT = moment(obj.startTime, 'hh:mm a');
          let eT = moment(obj.endTime, 'hh:mm a');
          if (eT.diff(sT, 'hours') < 0) {
            diffHrs = 24 + eT.diff(sT, 'hours');
          } else {
            diffHrs = eT.diff(sT, 'hours');
          }
          console.log('final result', diffHrs);
        }
      } else {
        // let fT = 12 - parseInt(fromTime);
        // diffHrs = fT + parseInt(endTime);
        let sT = moment(obj.startTime, 'hh:mm a');
        let eT = moment(obj.endTime, 'hh:mm a');
        if (eT.diff(sT, 'hours') < 0) {
          diffHrs = 24 + eT.diff(sT, 'hours');
        } else {
          diffHrs = eT.diff(sT, 'hours');
        }

      }
      console.log('diff horuse', diffHrs);
      if (diffHrs <= 4) {
        obj['callSh'] = 0.5;
      } else if (diffHrs <= 8) {
        obj['callSh'] = '1';
      } else if (diffHrs >= 8 && diffHrs <= 12) {
        obj['callSh'] = '1.5'
      } else if (diffHrs >= 12 && diffHrs <= 16) {
        obj['callSh'] = '2';
      } else if (diffHrs >= 16 && diffHrs <= 20) {
        obj['callSh'] = '2.5';
      } else if (diffHrs > 20) {
        obj['callSh'] = '3'
      }
      return obj['callSh'];
    } else {
      var init = obj.time.substring(obj.time.length - 1);
      if (init == '5') {
        obj['callSh'] = obj.time.substring(obj.time.length - 3);
        return obj.time.substring(obj.time.length - 3);
      } else {
        obj['callSh'] = obj.time.substring(obj.time.length - 1);
        return obj.time.substring(obj.time.length - 1);
      }
    }
  }
  dateFilterSearch() {

    let obj = {
      'fromDate': moment(this.tableFilter.fromDate.value._i).format('YYYY-MM-DD'),
      'toDate': moment(this.tableFilter.toDate.value._i).format('YYYY-MM-DD'),
      'offset': 0,
      'limit': 10,
      'filter_by': this.SearchByName
    }
    this.tmpDateFrom = moment(this.tableFilter.fromDate.value._i).format('YYYY-MM-DD');
    this.tempDateTo = moment(this.tableFilter.toDate.value._i).format('YYYY-MM-DD');
    this.getDataTable(obj);
  }
  dateClearSearch() {
    this.tmpDateFrom = '';
    this.tempDateTo = '';
    this.tableFilter.fromDate = new FormControl();
    this.tableFilter.toDate = new FormControl();
    let obj = {
      'fromDate': '1990-01-01',
      'toDate': moment(new Date()).format('YYYY-MM-DD'),
      'offset': 0,
      'limit': 10,
      'filter_by': this.SearchByName
    }
    this.getDataTable(obj);
  }
  getDataTable(filters) {
    this.loginService.getHistory(filters).subscribe(
      data => {
        this.totalLength = data['total'];
        this.dataSource = data['bills'];
      }, err => {
        this.toastr.error('Error', err.error.message);
        if (err.error.statusCode == '401') {
          localStorage.removeItem('cine-auth-check');
          this.Router.navigate(['/login']);
        }
      }
    )
  }
  onHourChange(event) {
    console.log('time change', event);
  }
  addEntries() {
    let obj;
    let length = this.inoviceObj.entries.length;
    obj = {
      uniqueId: this.inoviceObj.entries[length - 1].uniqueId + 1000,
      entryDate: new FormControl(moment().format('DD-MM-YYYY')),
      location: '',
      time: '',
      startTime: '',
      endTime: '',
      type: 'remove',
      subEntries: [
        {
          category: '',
          otherCategory:'',
          qty: '',
          cost: '',
          callsheet: '',
          total: 0,
          type: 'add'
        }
      ]
    };
    this.inoviceObj.entries.push(obj);
  }
  removeEntries(i) {
    console.log('entries', i);
    this.inoviceObj.entries.splice(i, 1);
  }
  getname() {
    return Math.floor(Math.pow(10, 10 - 1) + Math.random() * 9 * Math.pow(10, 10 - 1));
  }
  addSubEntries(i) {
    let obj;
    obj = {
      category: '',
      otherCategory:'',
      qty: '',
      cost: '',
      callsheet: '',
      total: 0,
      type: 'remove'
    }
    this.inoviceObj.entries[i].subEntries.push(obj)
  }
  removeSubEntries(mainind, subind) {
    this.inoviceObj.entries[mainind].subEntries.splice(subind, 1);
  }
  saveEntries() {
    if(this.validateConfig()) {
      this.apiCallLoader=true;
      var tempData = [];
      this.inoviceObj.entries.forEach(invObj => {
        var time;
        if (invObj.time == 'other') {
          invObj['timeOther'] = true;
          let sTime = invObj.startTime;
          let lTime = invObj.endTime;
          time =sTime.replace(':00', '') + ' to ' + lTime.replace(':00', '') ;
        } else {
          invObj['timeOther'] = false;
          time = invObj.time;
        }
        var daate;
        if(invObj.entryDate.status == 'INVALID') {
          daate='';
        } else {
          daate=moment(invObj.entryDate.value).format("YYYY-MM-DD");
        }
        invObj.subEntries.forEach(subObj => {
          let tmpObj = {
            "billItemUniqueId": invObj.uniqueId.toString(),
            "itemDate":daate,
            "itemName": subObj.category,
            "itemLocation": invObj.location,
            "time": invObj.time,
            "otherCategory":subObj.otherCategory,
            "startTime":invObj.startTime,
            "endTime":invObj.endTime,
            "quantity": subObj.qty,
            "price": subObj.cost,
            "callSheet": subObj.callsheet,
            "totalAmount": subObj.total,
            "id": subObj['id']
          };
          if (!this.Editable) {
            delete tmpObj['id'];
          }
          tempData.push(tmpObj);
        });
      });
      var reqObj = {
        "companyName": this.inoviceObj.companyName,
        "agentName": this.inoviceObj.agentName,
        "billDate": moment(this.inoviceObj.date.value).format("YYYY-MM-DD"),
        "billTitle": "Sample title",
        "language": this.inoviceObj.language,
        "billItems": tempData,
        "billAmount": this.getTotalFunction()
      }
      setTimeout(()=>{
        if (this.Editable && this.EditId != "") {
          this.loginService.updateData(reqObj, this.EditId).subscribe(
            data => {
              setTimeout(()=>{
                this.apiCallLoader=false;
              },1500);
              this.toastr.success('update successfully');
            }, err => {
                this.apiCallLoader=false;
             this.toastr.error('Error', err.error.message);
              if (err.error.statusCode == '401') {
                localStorage.removeItem('cine-auth-check');
                this.Router.navigate(['/login']);
              }
            }
          )
        } else {
          this.loginService.saveData(reqObj).subscribe(
            data => {
              setTimeout(()=>{
                this.apiCallLoader=false;
              },1500);
              this.toastr.success('saved successfully');
              this.clearDatas();
            }, err => {
                this.apiCallLoader=false;
              this.toastr.error('Error', err.error.message);
              if (err.error.statusCode == '401') {
                localStorage.removeItem('cine-auth-check');
                this.Router.navigate(['/login']);
              }
            }
          )
        }
      },2000);
    }
  }
  getamountwithfixed(amt) {
    return amt.toFixed(2);
  }
  printFunction() {
    if (this.validateConfig()) {
      var tempData = [];
      var mywindow = window.open('', 'PRINT', 'height=400,width=600');
      mywindow.document.write('<html xmlns="http://www.w3.org/1999/xhtml"><head runat="server"><title></title>');
      mywindow.document.write('<style type="text/css" media="print">@page {size: auto;margin: 0mm 10mm 10mm 10mm;}thead {display: table-header-group;}</style>');
      mywindow.document.write('<style type="text/css" media="screen"> thead { display: block; }tfoot { display: block; }.header-tr {}td { height:px !important; }tr{height:5px !important;padding:0px !important;margin:0px !important} </style> ');
      mywindow.document.write('</head>');
      mywindow.document.write(' <body><div style="margin:50mm 0mm 2.7mm 160mm; ">' + moment(this.inoviceObj.date.value).format("DD-MM-YYYY") + '</div><div style="margin:0.4mm 0mm 6mm 43mm">' + this.inoviceObj.companyName + '</div><div style="display:flex"><div style="margin:0mm 0mm 11mm 35mm">' + this.inoviceObj.agentName + '</div><div style="margin:0mm 0mm 0mm 140mm;position:absolute">' + this.inoviceObj.language + '</div></div> <form id="form1" runat="server"> <div> <table style="margin: 0;width: 100%;">   <tbody> ');
      mywindow.document.write('<tr style="width: 100%;"> ');
      mywindow.document.write('<th colspan="3" style="width: 60%"></th> ');
      mywindow.document.write('<th style="width: 10%;font-size:11px;">No</th> ');
      mywindow.document.write('<th style="width: 10%;font-size:11px;">Cost</th> ');
      mywindow.document.write('<th style="width: 10%;font-size:11px;">CallSheet</th>');
      mywindow.document.write('<th style="width: 10%;font-size:11px;">Total</th>');
      mywindow.document.write('</tr> ');
      this.inoviceObj.entries.forEach(invObj => {
        var time;
        var finalTime;
        if (invObj.time == 'other') {
          invObj['timeOther'] = true;
          if(invObj.startTime!='' && invObj.endTime!='') {
            let sTime = invObj.startTime;
            let lTime = invObj.endTime;
            time =sTime.replace(':00', '') + ' to ' + lTime.replace(':00', '') ;
          let startHour = parseInt(invObj.startTime.substr(0,2));
          let endHour = parseInt(invObj.endTime.substr(0,2));
          let startR = invObj.startTime.substring(2);
          let endR=invObj.endTime.substring(2);
           let start =  startHour+startR.replace(':00', '');
           let end =  endHour+endR.replace(':00', '');
           finalTime = start.replace(/ +/g, "")+' to '+end.replace(/ +/g, "");
          } else {
            time='';
            finalTime = '';
          } 
        } else {
          invObj['timeOther'] = false;
          finalTime =  invObj.time;
        }
        var daate;
        var printDate;
        console.log('invObj.location',invObj.location);
        console.log('invObj.date',invObj.entryDate);
        if(invObj.entryDate.status == 'INVALID') {
          daate='';
          printDate='';
        } else {
          daate=moment(invObj.entryDate.value).format("YYYY-MM-DD");
          printDate=moment(invObj.entryDate.value).format("DD-MM-YYYY");
        }
        mywindow.document.write(' <tr style="height:2px;"> ');
        mywindow.document.write('<td colspan="3" style="width: 60%;font-size:16px">' + printDate  + ' ' + invObj.location + ' ' + finalTime + '</td>');
        mywindow.document.write('</tr>');
        invObj.subEntries.forEach(subObj => {
          let tmpObj = {
            "billItemUniqueId": invObj.uniqueId.toString(),
            "itemDate": daate,
            "itemName": subObj.category,
            "itemLocation": invObj.location,
            "time": invObj.time,
            "otherCategory":subObj.otherCategory,
            "startTime":invObj.startTime,
            "endTime":invObj.endTime,
            "quantity": subObj.qty,
            "price": subObj.cost,
            "callSheet": subObj.callsheet,
            "totalAmount": subObj.total,
            "id": subObj['id']
          };
          if (!this.Editable) {
            delete tmpObj['id'];
          }
          tempData.push(tmpObj);
          var showSheetBool:boolean = true;
          let Findind =this.selectionCategory.indexOf(tmpObj.itemName);
             if(Findind == -1) {
              showSheetBool = false;
         } else {
              showSheetBool = true;
            }
            if(subObj.category == 'Others') {
              mywindow.document.write('<tr><td  colspan="3" style="width: 60%;font-size:14px""><span style="text-transform: uppercase"> ' + tmpObj.otherCategory + '</span> </td>');
            } else {
              mywindow.document.write('<tr><td  colspan="3" style="width: 60%;font-size:14px""><span style="text-transform: uppercase"> ' + tmpObj.itemName + '</span> </td>');
            }
          mywindow.document.write('<td  style="text-align: center;font-size:16px">' + subObj.qty + '</td>');
          mywindow.document.write('<td  style="text-align: center;font-size:16px">' + this.currencyFormat(subObj.cost) + '</td>');
          if(showSheetBool){
            mywindow.document.write('<td  style="text-align: center;font-size:16px">' + ' ' + '</td>');
          } else {
            mywindow.document.write('<td  style="text-align: center;font-size:16px">' + subObj['callsheet'] + '</td>');
          }
          mywindow.document.write('<td  style="text-align: right;font-size:16px">' + this.getTotalRs(invObj,subObj,subObj.callsheet) + '</td>');
          mywindow.document.write('</tr>');
        });
      });
      mywindow.document.write('</tbody>');
      mywindow.document.write('<tfoot> <tr> <th colspan="1"></th> <th colspan="1"></th>');
      mywindow.document.write('<th colspan="1"></th>');
      mywindow.document.write('<th colspan="1"></th>');
      mywindow.document.write('<th colspan="1"></th>');
      mywindow.document.write('<th colspan="0"></th><td style="border-top:1px solid black;text-align: right;" colspan="0">Rs.' + this.getTotalFunction() + '</td> </tr> </tfoot></table>');
      mywindow.document.write('</body></html>');
      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10*/
      mywindow.print();
      mywindow.close();
      var reqObj = {
        "companyName": this.inoviceObj.companyName,
        "agentName": this.inoviceObj.agentName,
        "billDate": moment(this.inoviceObj.date.value).format("YYYY-MM-DD"),
        "billTitle": "Sample title",
        "language": this.inoviceObj.language,
        "billItems": tempData,
        "billAmount": this.getTotalFunction(),
      }
      if (this.Editable && this.EditId != "") {
        this.apiCallLoader=true;
        this.loginService.updateData(reqObj, this.EditId).subscribe(
          data => {
            setTimeout(()=>{
              this.apiCallLoader=false;
            },1500);
            this.toastr.success('update successfully');
          }, err => {
              this.apiCallLoader=false;
            this.toastr.error('Error', err.error.message);
            if (err.error.statusCode == '401') {
              localStorage.removeItem('cine-auth-check');
              this.Router.navigate(['/login']);
            }
          }
        )
      } else {
        this.apiCallLoader=true;
        this.loginService.saveData(reqObj).subscribe(
          data => {
            setTimeout(()=>{
              this.apiCallLoader=false;
            },1500);
            this.toastr.success('saved successfully');
            this.clearDatas();
          }, err => {
              this.apiCallLoader=false;
            this.toastr.error('Error', err.error.message);
            if (err.error.statusCode == '401') {
              localStorage.removeItem('cine-auth-check');
              this.Router.navigate(['/login']);
            }
          }
        )
      }

      return true;
    }
  }
  clearCurrentSubEntries(mainObj,subObj) {
     this.inoviceObj.entries[mainObj].subEntries.pop();
  }
  tabClick($event) {
    // alert(this.SearchByName);
    this.tmpDateFrom = moment().subtract(1, "months").format("YYYY-MM-DD");
    this.tempDateTo = moment().format("YYYY-MM-DD");
    let obj;
    obj = {
      'fromDate': this.tmpDateFrom,
      'toDate': this.tempDateTo,
      'offset': 0,
      'limit': 10,
      'filter_by': this.SearchByName
    }

    // this.tableFilter.fromDate =new Date();
    this.getDataTable(obj);
  }
  searchByNameFilter() {
    let obj = {
      'fromDate': this.tmpDateFrom,
      'toDate': this.tempDateTo,
      'offset': 0,
      'limit': 10,
      'filter_by': this.SearchByName
    }
    this.loginService.getHistorySearch(obj).subscribe(
      data => {
        this.totalLength = data['total'];
        this.dataSource = data['bills'];
      }, err => {
        this.toastr.error('Error', err.error.message);
        if (err.error.statusCode == '401') {
          localStorage.removeItem('cine-auth-check');
          this.Router.navigate(['/login']);
        }
      }
    )
  }
  currencyFormat(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }
  // changeTimeValue(obj, ind) {
  //   this.inoviceObj.entries[ind].time = '9am to 6pm 1';
  // }
  changeCategoary(event,obj) {
    let Findind =this.selectionCategory.indexOf(event.value);
    if(Findind != -1) {
        obj.callsheet = '1';
    } else {
      obj.callsheet = '';
    }
  }
  clearDatas() {
    this.Editable = false;
    this.EditId = '';
    this.inoviceObj = {
      companyName: '',
      date: new FormControl(new Date()),
      agentName: '',
      language: '',
      entries: [
        {
          uniqueId: Math.floor(Math.pow(10, 10 - 1) + Math.random() * 9 * Math.pow(10, 10 - 1)),
          entryDate: new FormControl(new Date()),
          location: '',
          time: '',
          startTime: '',
          endTime: '',
          type: 'add',
          subEntries: [
            {
              category: '',
              otherCategory:'',
              qty: '',
              cost: '',
              callsheet: '',
              total: 0,
              type: 'add'
            }
          ]
        }
      ]
    }
  }
  validateConfig() {
    let errArr = [];
    if (this.inoviceObj.companyName == '') {
      errArr.push('companyName');
      this.toastr.error('Company Name Required');
    } else if (this.inoviceObj.agentName == '') {
      errArr.push('agentName');
      this.toastr.error('Agent Name Required');
    } else if (this.inoviceObj.date == undefined || this.inoviceObj.date == null) {
      this.toastr.error('Date is Required');
    } else if (this.inoviceObj.language == '') {
      errArr.push('language');
      this.toastr.error('Language Name Required');
    }else if(!this.inoviceObj.entries[0].entryDate) {
      errArr.push('entrydate');
      this.toastr.error('Bill date mandatory is first time');
    } else if(this.inoviceObj.entries[0]['location'] == '') {
      errArr.push('location');
      this.toastr.error('Location mandatory is first time');
    }else {
      for (let i=0;i<this.inoviceObj.entries.length;i++) {
        for(let j=0;j<this.inoviceObj.entries[i].subEntries.length;j++){
          let findInd = this.contractCateogary.indexOf(this.inoviceObj.entries[i].subEntries[j].category);
          if(findInd==-1) {
             if(this.inoviceObj.entries[i].time != ''){
               if(this.inoviceObj.entries[i].time == 'other') {
                if(this.inoviceObj.entries[i].startTime == '') {
                  errArr.push('time error');
                  this.toastr.error('start time missing please filled');
                  return false;
                  }
                 if(this.inoviceObj.entries[i].endTime == '') {
                  errArr.push('time error last');
                  this.toastr.error('end time missing please filled');
                  return false;
                }
               }
             } else {
              errArr.push('time error');
              this.toastr.error('time is missing please filled');
             }
          }
        if(this.inoviceObj.entries[i].subEntries[j].category =='') {
            errArr.push('categoary');
            this.toastr.error('Please fill all categoary field');
            return false;
        }  else if(this.inoviceObj.entries[i].subEntries[j].qty =='') {
          alert(this.inoviceObj.entries[i].subEntries[j].qty =='');
          errArr.push('Quantity');
          this.toastr.error('Please fill all Qty field');
          return false;
        } else if(this.inoviceObj.entries[i].subEntries[j].cost =='') {
          errArr.push('cost');
          this.toastr.error('Please fill all Cost field');
          return false;
        } else if(this.inoviceObj.entries[i].subEntries[j].callsheet == '') {
          errArr.push('callsheet');
          this.toastr.error('Please fill all callsheet  field');
          return false;
        } else if(this.inoviceObj.entries[i].subEntries[j].category == 'Others') {
          if(this.inoviceObj.entries[i].subEntries[j].otherCategory == '') {
            errArr.push('Othercategoary');
            this.toastr.error('Please fill Other categoary field');
            return false;
          }
        } 
        }
      }
        
    }

    if (errArr.length == 0) {
      return true;
    } else {
      return false;
    }
  }
  onKey(event, obj, sheet) {
    let costQty = parseInt(obj.cost) * sheet;
    obj.total = parseInt(obj.qty) * costQty;
  }
  getTotalRs(event, obj, sheet) {
    let costQty = parseInt(obj.cost) * sheet;
    obj.total = parseInt(obj.qty) * costQty;
    if (!obj.total) {
      obj.total = 0;
      return 0;
    } else {
      // let totalmul = obj.total *100;
      // let totaldiv = totalmul /100;
      return obj.total.toFixed(2);
    }
  }
  getTotalFunction() {
    let TotalAmount = 0;
    this.inoviceObj.entries.forEach(invObj => {
      invObj.subEntries.forEach(subObj => {
        TotalAmount = TotalAmount + subObj.total;
      });
    });
    let mul= TotalAmount * 100;
    let div = mul/100;
    return div.toFixed(2);
  }
  logoutFunction() {
    this.loginService.logoutService().subscribe(
      data => {
        localStorage.clear();
        this.Router.navigate(['/login']);
        this.toastr.success('success', data['message']);
      },
      err => {
        localStorage.clear();
        this.toastr.error('success', err.error['message']);
        this.Router.navigate(['/login']);
      }
    )
    localStorage.removeItem('cine-auth-check');
  }
  editFunction(id) {
    this.inoviceObj = {
      companyName: '',
      date: new FormControl(new Date()),
      agentName: '',
      language: '',
      entries: [
        {
          uniqueId: Math.floor(Math.pow(10, 10 - 1) + Math.random() * 9 * Math.pow(10, 10 - 1)),
          entryDate: new FormControl(new Date()),
          location: '',
          time: '',
          startTime: '',
          endTime: '',
          type: 'add',
          subEntries: [
            {
              category: '',
              otherCategory:'',
              qty: '',
              cost: '',
              callsheet: '',
              total: 0,
              type: 'add'
            }
          ]
        }
      ]
    }
    this.Editable = true;
    this.EditId = id;
    this.loginService.getHistoryDetail(id).subscribe(
      data => {
        var dataObj;
        dataObj = data;
        let groupbyValue = _.groupBy(dataObj.billItems, 'billItemUniqueId');
        var objectKeyLength = Object.keys(groupbyValue).length
        var tempData = [];
        this.inoviceObj.companyName = dataObj.companyName;
        this.inoviceObj.agentName = dataObj.agentName;
        this.inoviceObj.language = dataObj.language;
        let tmpD = new Date(moment(dataObj.billDate).format());
        this.inoviceObj.date = new FormControl(moment(tmpD).format());
        console.log('form data', dataObj.itemDate);
        var i = 0;
        for (var key in groupbyValue) {

          let timeInd = this.displayTime.indexOf(groupbyValue[key][0].time);
          if (timeInd == -1) {
            this.inoviceObj.entries[i].startTime = groupbyValue[key][0].startTime;
            this.inoviceObj.entries[i].endTime = groupbyValue[key][0].endTime
            this.inoviceObj.entries[i].time = groupbyValue[key][0].time
          } else {
            this.inoviceObj.entries[i].time = groupbyValue[key][0].time
            this.inoviceObj.entries[i].startTime = '';
            this.inoviceObj.entries[i].endTime = ''
          }
          this.inoviceObj.entries[i]['uniqueId'] = groupbyValue[key][0].billItemUniqueId;
          let tmpDD = groupbyValue[key][0].itemDate+'T00:00:00.000Z';
          this.inoviceObj.entries[i].entryDate = new FormControl(moment(tmpDD));
          this.inoviceObj.entries[i].location = groupbyValue[key][0].itemLocation;
          this.inoviceObj.entries[i].type = i == 0 ? 'add' : 'remove';
          this.inoviceObj.entries[i].subEntries = []
          // this.inoviceObj.entries.push(obj);
          groupbyValue[key].forEach(subObj => {
            let catInd = this.categoryDisp.indexOf(groupbyValue[key][0].itemName);
            let obj = {
              category: subObj.itemName,
              otherCategory:subObj.otherCategory,
              qty: subObj.quantity,
              cost: subObj.price,
              callsheet: subObj.callSheet,
              total: subObj.totalAmount,
              type: 'add'
            }
            obj['id'] = subObj['id'];
            this.inoviceObj.entries[i].subEntries.push(obj);
          });
          let obb = {
            uniqueId: Math.floor(Math.pow(10, 10 - 1) + Math.random() * 9 * Math.pow(10, 10 - 1)),
            entryDate: new FormControl(new Date()),
            location: '',
            time: '',
            startTime: '',
            endTime: '',
            type: i == 0 ? 'add' : 'remove',
            subEntries: []
          }
          if (i + 1 != objectKeyLength) {
            this.inoviceObj.entries.push(obb);
          }
          this.selected = new FormControl(1);
          i++;
        }
      }, err => {
        this.toastr.error('Error', err.error.message);
        if (err.error.statusCode == '401') {
          localStorage.removeItem('cine-auth-check');
          this.Router.navigate(['/login']);
        }
      }
    )
    //  this.inoviceObj.entries.splice(0,1);
  }
  deleteFunction(id) {
    var r = confirm("Are you Sure want to delete?");
  if (r == true) {
    this.loginService.deleteBill(id).subscribe(data=>{
      this.toastr.success('delete Successfully');
      this.tmpDateFrom = moment().subtract(1, "months").format("YYYY-MM-DD");
      this.tempDateTo = moment().format("YYYY-MM-DD");
      let obj;
      obj = {
        'fromDate': this.tmpDateFrom,
        'toDate': this.tempDateTo,
        'offset': 0,
        'limit': 10,
        'filter_by': this.SearchByName
      }
  
      // this.tableFilter.fromDate =new Date();
      this.getDataTable(obj);
    },err=>{
      this.toastr.error('Error', err.error.message);
      if (err.error.statusCode == '401') {
        localStorage.removeItem('cine-auth-check');
        this.Router.navigate(['/login']);
      }
    }) 
  } else {
  }
  }
  timeChange() {
    // alert('hi');
  }

}
