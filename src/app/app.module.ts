import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './login/login.component';
import {MatProgressSpinnerModule,MatTooltipModule,MatChipsModule,MatInputModule,MatButtonModule,MatCardModule,MatToolbarModule,MatIconModule,MatTabsModule,MatDatepickerModule,MatNativeDateModule,MatSelectModule, MatTableModule, MatPaginatorModule, MatSortModule} from '@angular/material';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { InvoicePageComponent } from './invoice-page/invoice-page.component';
import {DataTableModule} from "angular-6-datatable";
import { NumbersonlyDirective } from './directive/numbersonly.directive';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { loginService } from './services/apiCall.service';
import { AuthGuardService } from './services/auth-gaurd.service';
import { from } from 'rxjs';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';  
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InvoicePageComponent,
    NumbersonlyDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    MatToolbarModule,
    MatIconModule,
    MatTabsModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    DataTableModule,
    NgxDatatableModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatChipsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    NgxMaterialTimepickerModule.forRoot(),
    MatTooltipModule,
    MatDatepickerModule,
    MatProgressSpinnerModule
  ],
  providers: [
    MatNativeDateModule,
    loginService,
    AuthGuardService,
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
